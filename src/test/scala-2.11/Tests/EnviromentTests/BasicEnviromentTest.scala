package Tests.EnviromentTests

import Missions.{Enviroment, Mission}
import Skills.Skill
import Survivor.{Group, Survivor}
import Tests.ScalaTestBase

import scala.collection.mutable.Stack

/**
  * Created by Wysocki on 13/05/2016.
  */
class BasicEnviromentTest extends ScalaTestBase {

	trait TrapGroup {
		var Skills1 : Map[String, Int] = Map()
		Skills1 += ("traps" -> 1)
		Skills1 += ("explosives" -> 1)
		Skills1 += ("first aid" -> 2)
		val Ocal = new Survivor(5, Skills1)
		val Ocal2 = new Survivor(3, Skills1)
		val Ocaleni = List(Ocal, Ocal2)
		var Grupa = new Group(Ocaleni)
	}

	trait TrapMissionLevel8 {
		var Skills2 : Set[Skill] = Set()
		Skills2 += new Skill("traps")
		Skills2 += new Skill("explosives")
		var EnviroSegment = new Enviroment(8, Skills2)
	}
	trait TrapMissionLevel16 {
		var Skills2 : Set[Skill] = Set()
		Skills2 += new Skill("traps")
		Skills2 += new Skill("explosives")
		var EnviroSegment = new Enviroment(10, Skills2)
	}

//	"A Stack" should "pop values in last-in-first-out order" in {
//		val stack = new Stack[Int]
//		stack.push(1)
//		stack.push(2)
//		assert(stack.pop === 2)
//		assert(stack.pop === 1)
//	}
//
//	it should "throw NoSuchElementException if an empty stack is popped" in {
//		val emptyStack = new Stack[String]
//		intercept[NoSuchElementException] {
//			emptyStack.pop
//		}
//	}

	"A group consisting of men with traps skill on level 4" should " pass the trap test of level (8) roughly quarter of a time " in new TrapGroup with TrapMissionLevel8 {
		var SuccessAmmount = 0
		var AllTests = 1000
		for(a <- 0 to AllTests) {
			if (EnviroSegment.Evaluate(Grupa)) SuccessAmmount += 1
		}
		assert(SuccessAmmount > 500)
	}
	it should " pass the trap test of level (16) roughly half the time " in new TrapGroup with TrapMissionLevel16 {
		var SuccessAmmount = 0
		var AllTests = 1000
		for(a <- 0 to AllTests) {
			if (EnviroSegment.Evaluate(Grupa)) SuccessAmmount += 1
		}
		assert(SuccessAmmount > 250)
	}


}
