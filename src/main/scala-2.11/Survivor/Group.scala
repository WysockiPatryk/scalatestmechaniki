package Survivor

import Skills.Skill

/**
  * Created by Wysocki on 10/05/2016.
  */
class Group (Survivors : List[Survivor]) {
	def AllSkills : Map[String, Int] = {
		var mapa = Map[String, Int]()

		for(surv <- Survivors) {
			for(skill <- surv.Skills) {
				var potentialSkill = mapa.get(skill._1)
				if(potentialSkill.isEmpty) {
					mapa += (skill._1 -> skill._2)
				} else {
					var foundSkill = potentialSkill.get
					foundSkill += skill._2
					mapa = mapa.updated(skill._1, foundSkill)
				}
			}
		}
		return mapa
	}
}
