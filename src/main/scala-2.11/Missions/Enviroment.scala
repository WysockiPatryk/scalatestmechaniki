package Missions
import Survivor.Group
import Skills.Skill;
/**
  * Created by Wysocki on 10/05/2016.
  */
class Enviroment (var Difficulty : Int, var SkillReq : Set[Skill]) extends Segment{

	override def Evaluate(group: Group): Boolean = {
		val Skille = group.AllSkills
		val DobreSkille = Skille.filterKeys((Skill) => SkillReq.exists((ReqSkill) => ReqSkill.Name == Skill))

		val CombinedSkillLevel = DobreSkille.values.sum

		val r = scala.util.Random
		var result = 0;
		for( a <- 1 to CombinedSkillLevel){
			result += r.nextInt(4) + 1
		}
		return result > Difficulty
	}
}
