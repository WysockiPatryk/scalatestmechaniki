package Missions

import Survivor.Group;

/**
  * Created by Wysocki on 10/05/2016.
  */
class Mission (MissionName : String,
               Segments : List[Segment]) {
	def BeginMission(group : Group) = {
		for (seg <- Segments) {
			seg.Evaluate(group)
		}
	}
	def ContinueMission() = {

	}
}
