package Missions

import Survivor.Group;

/**
  * Created by Wysocki on 10/05/2016.
  */
trait Segment {
	def Evaluate(group : Group) : Boolean
}
